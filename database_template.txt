const mariadb = require('mariadb');

async function getData() {
  let conn;

  try {
    conn = await mariadb.createConnection({
      host: 'localhost',
      user: 'your_username',
      password: 'your_password',
      database: 'your_database_name'
    });

    const rows = await conn.query('SELECT * FROM your_table_name');

    console.log(rows);
  } catch (err) {
    throw err;
  } finally {
    if (conn) conn.end();
  }
}

getData();
