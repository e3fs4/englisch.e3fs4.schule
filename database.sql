CREATE DATABASE english;
USE english;

CREATE TABLE Comment(
    CommentID INT PRIMARY KEY AUTO_INCREMENT,
    Comment TEXT,
    IsValid TINYINT(1)
);