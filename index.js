import dotenv from 'dotenv';
import express from 'express';
import * as path from 'path';

const __dirname = path.resolve();
const ip = process.env.IP;
const port = process.env.PORT;
const rIntervall = process.env.REFRESH_INTERVALL;

// This is important for the .env Configuration file!
dotenv.config();

function intervalFunc() {
}
setInterval(intervalFunc, rIntervall)

const app = express();

app.use(express.static(path.join(__dirname, '/src')));

app.get('/', async function(req, res) {
    res.sendFile(path.join(__dirname, '/src/main.html'));
});

app.listen(port, ip);
console.log(`Server running at ${ip}:${port}`);